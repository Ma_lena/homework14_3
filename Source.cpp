#include <iostream>
#include <string>

int main()
{
	std::string first = "i`m a programmer";

	std::cout << first << "\n";
	std::cout << first.length() << "\n";
	std::cout << first.front() << "\n";
	std::cout << first.back() << "\n";
}